package pojo.request.glossary;

import lombok.*;

@AllArgsConstructor
@Getter
public class Glossary {
    private final GlossaryData glossaryData;
}
